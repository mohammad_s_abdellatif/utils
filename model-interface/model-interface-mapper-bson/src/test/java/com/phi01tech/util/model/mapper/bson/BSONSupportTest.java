package com.phi01tech.util.model.mapper.bson;

import com.phi01tech.utils.model.mapper.GetterPropertyNameExtractor;
import com.phi01tech.utils.model.mapper.ModelMapper;
import com.phi01tech.utils.model.mapper.ModelMapperBuilder;
import com.phi01tech.utils.model.mapper.PropertyNameExtractor;
import org.bson.*;
import org.bson.types.ObjectId;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by phi01tech on 5/30/17.
 */
public class BSONSupportTest {

    @Test
    public void successWhenPassObjectId() {
        ModelMapper modelMapper = constructMapperWithBSONConvertor();

        Document document = new Document();
        ObjectId objectId = new ObjectId();
        document.append("id", objectId);
        document.append("firstName", "Mohammad");
        document.append("lastName", "Abdellatif");

        Employee employee = modelMapper.mapModel(document, Employee.class);
        assertNotNull(employee);
        Assert.assertEquals(objectId.toHexString(), employee.getId());
        Assert.assertEquals("Mohammad", employee.getFirstName());
        Assert.assertEquals("Abdellatif", employee.getLastName());
    }


    @Test
    public void successWhenPassObjectIdWithStandardIdName() {
        ModelMapper modelMapper = constructBSONMapper();

        Document document = new Document();
        ObjectId objectId = new ObjectId();
        document.append("_id", objectId);
        document.append("firstName", "Mohammad");
        document.append("lastName", "Abdellatif");

        Employee employee = modelMapper.mapModel(document, Employee.class);
        assertNotNull(employee);
        Assert.assertEquals(objectId.toHexString(), employee.getId());
        Assert.assertEquals("Mohammad", employee.getFirstName());
        Assert.assertEquals("Abdellatif", employee.getLastName());
    }


    @Test
    public void successWhenPassArrayThenMapItToArray() {
        ModelMapper modelMapper = constructBSONMapper();

        Document document = new Document();
        ObjectId objectId = new ObjectId();
        document.append("_id", objectId);
        document.append("firstName", "Mohammad");
        document.append("lastName", "Abdellatif");
        document.append("hobbies", new BsonArray(Arrays.asList(new BsonString("Swimming"), new BsonString("Hiking"))));
        document.append("hobbiesAsList", new BsonArray(Arrays.asList(new BsonString("Swimming"), new BsonString("Hiking"))));
        document.append("hobbiesAsSet", new BsonArray(Arrays.asList(new BsonString("Swimming"), new BsonString("Hiking"))));
        document.append("hobbiesAsCollection", new BsonArray(Arrays.asList(new BsonString("Swimming"), new BsonString("Hiking"))));

        Employee employee = modelMapper.mapModel(document, Employee.class);
        assertNotNull(employee);
        assertEquals("Mohammad", employee.getFirstName());
        assertEquals("Abdellatif", employee.getLastName());
        assertEquals(objectId.toHexString(), employee.getId());
        assertEquals(Arrays.asList("Swimming", "Hiking"), employee.getHobbiesAsList());
        assertArrayEquals(new String[]{"Swimming", "Hiking"}, employee.getHobbies());
        assertEquals(new HashSet<>(Arrays.asList("Swimming", "Hiking")), employee.getHobbiesAsSet());
    }

    @Test
    public void successWhenPassArrayThenMapItToList() {
        ModelMapper modelMapper = constructBSONMapper();

        Document document = new Document();
        document.append("hobbiesAsList", new BsonArray(Arrays.asList(new BsonString("Swimming"), new BsonString("Hiking"))));

        Employee employee = modelMapper.mapModel(document, Employee.class);
        assertNotNull(employee);
        assertEquals(Arrays.asList("Swimming", "Hiking"), employee.getHobbiesAsList());
    }

    @Test
    public void successWhenPassBsonStringValueThenMap() {
        ModelMapper modelMapper = constructBSONMapper();

        Document document = new Document();
        ObjectId objectId = new ObjectId();
        document.append("_id", objectId);
        document.append("firstName", new BsonString("Mohammad"));
        document.append("lastName", new BsonString("Abdellatif"));

        Employee employee = modelMapper.mapModel(document, Employee.class);
        assertNotNull(employee);
        assertEquals("Mohammad", employee.getFirstName());
        assertEquals("Abdellatif", employee.getLastName());
        assertEquals(objectId.toHexString(), employee.getId());
    }

    @Test
    public void successWhenPassBsonInt32ThenMap() {
        ModelMapper modelMapper = constructBSONMapper();
        Document document = new Document();
        document.append("x", new BsonInt32(100));
        document.append("y", new BsonInt32(30));
        Point point = modelMapper.mapModel(document, Point.class);
        assertNotNull(point);
        assertEquals(100, point.getX());
        assertEquals(30, point.getY());
    }

    @Test
    public void successWhenPassBsonDoubleThenMap() {
        ModelMapper modelMapper = constructBSONMapper();
        Document document = new Document();
        document.append("latitude", new BsonDouble(34.5));
        document.append("longitude", new BsonDouble(35.5));
        Location point = modelMapper.mapModel(document, Location.class);
        assertNotNull(point);
        assertEquals(34.5, point.getLatitude(), 0);
        assertEquals(35.5, point.getLongitude(), 0);
    }

    @Test
    public void successWhenPassBsonInt46ThenMap() {
        ModelMapper modelMapper = constructBSONMapper();
        long value = System.currentTimeMillis();
        Document document = new Document();
        document.append("timeStamp", new BsonInt64(value));
        document.append("timeStampAsObj", new BsonInt64(value));
        Timestamp point = modelMapper.mapModel(document, Timestamp.class);
        assertNotNull(point);
        assertEquals(value, point.getTimeStamp());
        assertEquals(Long.valueOf(value), point.getTimeStampAsObj());
    }


    private ModelMapper constructMapperWithBSONConvertor() {
        ModelMapperBuilder builder = new ModelMapperBuilder();
        builder.appendValueConvertor(new BsonValueConverter());
        return builder.build();
    }

    private ModelMapper constructBSONMapper() {
        ModelMapperBuilder builder = new ModelMapperBuilder();
        builder.appendValueConvertor(new BsonValueConverter());
        PropertyNameExtractor propertyNameExtractor = new GetterPropertyNameExtractor();
        builder.setPropertyNameExtractor(new BsonPropertyNameExtractor(propertyNameExtractor));
        return builder.build();
    }

    public interface Employee {
        String getId();

        String getFirstName();

        String getLastName();

        String[] getHobbies();

        List<String> getHobbiesAsList();

        Set<String> getHobbiesAsSet();

        Collection<String> getHobbiesAsCollection();
    }

    public interface Point {
        int getX();

        short getY();
    }

    public interface Location {
        double getLatitude();

        double getLongitude();
    }

    public interface Timestamp {
        long getTimeStamp();

        Long getTimeStampAsObj();
    }
}
