package com.phi01tech.util.model.mapper.bson;

import java.lang.reflect.Array;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.bson.BsonArray;
import org.bson.BsonDateTime;
import org.bson.BsonNumber;
import org.bson.BsonString;
import org.bson.types.ObjectId;

import com.phi01tech.utils.model.mapper.FunctionalValueConverter;
import com.phi01tech.utils.model.mapper.ValueConversionException;
import com.phi01tech.utils.model.mapper.ValueConverter;
import com.phi01tech.utils.model.mapper.ValueConverterDelegate;

/**
 * Created by phi01tech on 5/30/17.
 */
public class BsonValueConverter extends ValueConverterDelegate {

	protected final List<ValueConverter> bsonConvertors = new ArrayList<>();

	{
		bsonConvertors.add(FunctionalValueConverter.with((st, vt) -> ObjectId.class.isAssignableFrom(st),
				(v, vt) -> ((ObjectId) v).toHexString()));
		bsonConvertors.add(FunctionalValueConverter.with((st, vt) -> BsonString.class.isAssignableFrom(st),
				(v, vt) -> ((BsonString) v).getValue()));
		bsonConvertors.add(FunctionalValueConverter.with((st, vt) -> BsonNumber.class.isAssignableFrom(st),
				this::convertBsonNumber));
		bsonConvertors.add(FunctionalValueConverter.with((st, vt) -> BsonArray.class.isAssignableFrom(st),
				this::convertBsonArray));
		bsonConvertors.add(FunctionalValueConverter.with((st, vt) -> BsonDateTime.class.isAssignableFrom(st),
				this::convertBsonDateTime));

	}

	@Override
	protected Iterable<ValueConverter> getEmbeddedConvertors() {
		return bsonConvertors;
	}

	protected Object convertBsonDateTime(Object v, Class vt) {
		BsonDateTime bdt = (BsonDateTime) v;
		long epochSecond = bdt.getValue();
		if (vt == Date.class)
			return new Date(epochSecond);
		if (vt == java.sql.Date.class)
			return new java.sql.Date(epochSecond);
		LocalDateTime localDateTime = LocalDateTime.ofEpochSecond(epochSecond, 0, ZoneOffset.UTC);
		if (vt == LocalDateTime.class)
			return localDateTime;
		if (vt == LocalDate.class)
			return localDateTime.toLocalDate();
		throw new ValueConversionException("unable to map BsonDateTime: " + v);
	}

	protected Object convertBsonNumber(Object v, Class vt) {
		BsonNumber num = (BsonNumber) v;
		if (vt == Integer.class || vt == Integer.TYPE)
			return num.intValue();
		if (vt == Double.class || vt == Double.TYPE)
			return num.doubleValue();
		if (vt == Long.class || vt == Long.TYPE)
			return num.longValue();
		if (vt == Short.class || vt == Short.TYPE)
			return (short) num.intValue();
		throw new ValueConversionException("Unable to map BsonNumber: " + v);
	}

	protected Object convertBsonArray(Object value, Class valueType) {
		BsonArray bsonArray = (BsonArray) value;
		List<Object> values = new ArrayList<>();
		// TODO fix Object.class, there is no way to get the generic type of a
		// returned value
		// for now, BsonArray can only contains BsonValue
		bsonArray.forEach((bv) -> values.add(BsonValueConverter.this.convert(bv, Object.class)));

		if (valueType.isArray())
			return returnAsArray(valueType, values);
		if (LinkedList.class.isAssignableFrom(valueType))
			return new LinkedList<>(values);
		if (Set.class.isAssignableFrom(valueType) || HashSet.class.isAssignableFrom(valueType))
			return new HashSet<>(values);
		if (LinkedHashSet.class.isAssignableFrom(valueType))
			return new LinkedHashSet<>(values);
		if (SortedSet.class.isAssignableFrom(valueType) || TreeSet.class.isAssignableFrom(valueType))
			return new TreeSet<>(values);
		if (List.class.isAssignableFrom(valueType) || Collection.class.isAssignableFrom(valueType))
			return values;
		throw new ValueConversionException("Unable to convert BsonArray to object of type: " + valueType);
	}

	protected Object returnAsArray(Class valueType, List<Object> values) {
		Class type = valueType.getComponentType();
		Object array = Array.newInstance(type, values.size());
		for (int i = 0; i < values.size(); i++) {
			Array.set(array, i, values.get(i));
		}
		return array;
	}
}
