package com.phi01tech.util.model.mapper.bson;

import java.lang.reflect.Method;

import com.phi01tech.utils.model.mapper.NotPropertyReadMethodException;
import com.phi01tech.utils.model.mapper.PropertyNameExtractor;

/**
 * Created by phi01tech on 5/30/17.
 */
public class BsonPropertyNameExtractor implements PropertyNameExtractor {

    private PropertyNameExtractor propertyNameExtractor;

    public BsonPropertyNameExtractor(PropertyNameExtractor propertyNameExtractor) {
        this.propertyNameExtractor = propertyNameExtractor;
    }

    @Override
    public String extractPropertyName(Class<?> mappedType, Method method) throws NotPropertyReadMethodException {
        String propertyName = propertyNameExtractor.extractPropertyName(mappedType, method);
        if (propertyName.equals("id"))
            return "_id";
        return propertyName;
    }
}
