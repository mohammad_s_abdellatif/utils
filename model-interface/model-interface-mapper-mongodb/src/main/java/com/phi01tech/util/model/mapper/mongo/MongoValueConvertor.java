package com.phi01tech.util.model.mapper.mongo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import com.mongodb.DBRef;
import com.phi01tech.util.model.mapper.bson.BsonValueConverter;
import com.phi01tech.utils.model.mapper.FunctionalValueConverter;
import com.phi01tech.utils.model.mapper.ValueConversionException;

public class MongoValueConvertor extends BsonValueConverter {

	public MongoValueConvertor() {
		bsonConvertors.add(FunctionalValueConverter.with(
				(st, vt) -> DBRef.class.isAssignableFrom(st),
				this::convertDBRef));
		bsonConvertors.add(FunctionalValueConverter.with((st, vt) -> ArrayList.class.isAssignableFrom(st),
				this::convertArrayList));
	}

	private String convertDBRef(Object value, Class type) {
		DBRef ref = (DBRef) value;
		return ref.getId().toString();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected Object convertArrayList(Object value, Class valueType) {
		ArrayList list = (ArrayList) value;
		List values = new ArrayList<>();
		list.forEach((bv) -> values.add(convert(bv, Object.class)));

		if (valueType.isArray())
			return returnAsArray(valueType, values);
		if (LinkedList.class.isAssignableFrom(valueType))
			return new LinkedList<>(values);
		if (Set.class.isAssignableFrom(valueType) || HashSet.class.isAssignableFrom(valueType))
			return new HashSet<>(values);
		if (LinkedHashSet.class.isAssignableFrom(valueType))
			return new LinkedHashSet<>(values);
		if (SortedSet.class.isAssignableFrom(valueType) || TreeSet.class.isAssignableFrom(valueType))
			return new TreeSet<>(values);
		if (List.class.isAssignableFrom(valueType) || Collection.class.isAssignableFrom(valueType))
			return values;

		throw new ValueConversionException("unable to convert ArrayList to object of type: " + valueType);
	}
}
