package com.phi01tech.utils.model.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by phi01tech on 5/28/17.
 */
public class ValueConverterDelegate implements ValueConverter {

	private List<ValueConverter> convertors = new ArrayList<>();

	protected ValueConverterDelegate() {

	}

	public ValueConverterDelegate(List<ValueConverter> convertors) {
		this.convertors = new ArrayList<>(convertors);
	}

	public boolean supports(Class<?> sourceType, Class<?> valueType) {
		for (ValueConverter convertor : getEmbeddedConvertors()) {
			if (convertor.supports(sourceType, valueType))
				return true;
		}
		return false;
	}

	@Override
	public <T> T convert(Object value, Class<T> valueType) throws ValueConversionException {
		ValueConversionException lastException = null;
		for (ValueConverter convertor : getEmbeddedConvertors()) {
			try {
				if (convertor.supports(value.getClass(), valueType))
					return convertor.convert(value, valueType);
			} catch (ValueConversionException e) {
				Logger.getLogger(ValueConverterDelegate.class.getName())
						.warning("unable to convert, look for another convertor: " + e.getMessage());
				lastException = e;
			}
		}
		throw new ValueConversionException("unable to convert value: " + value + " to " + valueType, lastException);
	}

	protected Iterable<ValueConverter> getEmbeddedConvertors() {
		return convertors;
	}
}
