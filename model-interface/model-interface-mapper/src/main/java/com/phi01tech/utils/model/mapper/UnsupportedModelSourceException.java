package com.phi01tech.utils.model.mapper;

/**
 * Created by phi01tech on 5/27/17.
 */
public class UnsupportedModelSourceException extends ModelMappingException {
    public UnsupportedModelSourceException(String message) {
        super(message);
    }
}
