package com.phi01tech.utils.model.mapper;

import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * Created by phi01tech on 5/27/17.
 */
public class DefaultObjectMethodsHandler implements ObjectMethodsHandler {

    public Object handle(Object proxy, Method method, Object[] args) throws Exception {
        String name = method.getName();
        if (name.equals("equals"))
            return proxy == args[0];
        if (name.equals("hashCode"))
            return proxy.getClass().getCanonicalName().hashCode();
        if (name.equals("toString"))
            return "Generated Model Interface: " + Arrays.toString(proxy.getClass().getInterfaces());
        throw new IllegalStateException("unable to handle object methods");
    }
}
