package com.phi01tech.utils.model.mapper;

import java.util.LinkedList;

/**
 * Defines the context of model mapping, custom convertors and extractors
 * can get a reference for the ModelMapper invoking them.
 */
public abstract class MappingContext {

    private static final ThreadLocal<MappingContextChain> contexts = new ThreadLocal<>();

    public static void setContext(MappingContext mappingContext) {
        if (contexts.get() != null)
            throw new IllegalStateException("Context already set exception");
        MappingContextChain contextChain = new MappingContextChain();
        contextChain.contexts.add(mappingContext);
        contexts.set(contextChain);
    }

    public static void embedContext(MappingContext mappingContext) {
        if (contexts.get() == null)
            throw new IllegalStateException("No root context found");
        contexts.get().contexts.add(mappingContext);
    }

    public static MappingContext getContext() {
        if (contexts.get() != null) {
            MappingContextChain contextChain = contexts.get();
            MappingContext current = contextChain.contexts.peekLast();
            if (current != null)
                return current;
        }
        throw new IllegalStateException("no context was setup");
    }

    public static void clearCurrent() {
        MappingContextChain chain = contexts.get();
        if (chain != null) {
            if (!chain.contexts.isEmpty())
                chain.contexts.pop();
            if (chain.contexts.isEmpty())
                contexts.remove();
        }
    }

    /**
     * Get the current model mapper processing the mapping request.
     *
     * @return
     */
    abstract ModelMapper getModelMapper();

    /**
     * Get the source of mapping, root source
     *
     * @return
     */
    abstract Object getModelSource();

    /**
     * The target model interface
     *
     * @return
     */
    abstract Class getModelInterface();

    private static class MappingContextChain {
        LinkedList<MappingContext> contexts = new LinkedList<>();
    }
}