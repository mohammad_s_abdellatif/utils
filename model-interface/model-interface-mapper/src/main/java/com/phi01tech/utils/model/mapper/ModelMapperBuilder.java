package com.phi01tech.utils.model.mapper;

import java.util.ArrayList;
import java.util.List;

/**
 * A builder for {@link DefaultModelMapper} instances,
 * invoking {@link #build()} method directly will return a mapper
 * with default settings.
 * <p>
 * Created by phi01tech on 5/28/17.
 */
public class ModelMapperBuilder {

    private PropertyNameExtractor propertyNameExtractor = new GetterPropertyNameExtractor();
    private ObjectMethodsHandler objectMethodsHandler = new DefaultObjectMethodsHandler();
    private List<ValueConverter> valueConverters = new ArrayList<>();
    private boolean appendDefaultValueConvertor = true;

    /**
     * Set the strategy for extracting a property key when a method is invoked
     * on the model interface instance.
     *
     * @param propertyNameExtractor property key extraction strategy
     * @return
     */
    public ModelMapperBuilder setPropertyNameExtractor(PropertyNameExtractor propertyNameExtractor) {
        this.propertyNameExtractor = propertyNameExtractor;
        return this;
    }

    /**
     * Override th default {@link ObjectMethodsHandler} which is responsible to provide
     * implementation for Object methods: equals, toString, and hashCode.
     *
     * @param objectMethodsHandler custom object methods handler.
     * @return this builder
     */
    public ModelMapperBuilder setObjectMethodsHandler(ObjectMethodsHandler objectMethodsHandler) {
        this.objectMethodsHandler = objectMethodsHandler;
        return this;
    }

    /**
     * Set the builder if it shall include the default {@link ValueConverter} used by
     * generated implementation or not.
     *
     * @param append
     * @return
     */
    public ModelMapperBuilder appendDefaultValueConvertor(boolean append) {
        this.appendDefaultValueConvertor = append;
        return this;
    }

    /**
     * Append a value convertor before the default value convertor if
     * {@link #appendDefaultValueConvertor(boolean)} is set to <code>true</code>.
     * Appended convertors will be invoked befor the default convertor
     * is invoked to allow customization of conversion.
     *
     * @param convertor
     * @return
     */
    public ModelMapperBuilder appendValueConvertor(ValueConverter convertor) {
        valueConverters.add(convertor);
        return this;
    }

    /**
     * Construct an instance of {@link DefaultModelMapper} according to the builder
     * setup.
     *
     * @return mapper customized instance.
     */
    public ModelMapper build() {
        DefaultModelMapper mapper = new DefaultModelMapper();
        ArrayList<ValueConverter> convertors = new ArrayList<>(valueConverters);
        ValueConverter delegator;
        PropertyValueExtractor propertyValueExtractor;

        if (appendDefaultValueConvertor)
            convertors.add(new ConvertUtilsValueConverter());
        delegator = new ValueConverterDelegate(convertors);

        propertyValueExtractor = new DefaultPropertyValueExtractor(delegator);

        mapper.setObjectMethodsHandler(objectMethodsHandler);
        mapper.setPropertyNameExtractor(propertyNameExtractor);
        mapper.setPropertyValueExtractor(propertyValueExtractor);
        return mapper;
    }
}
