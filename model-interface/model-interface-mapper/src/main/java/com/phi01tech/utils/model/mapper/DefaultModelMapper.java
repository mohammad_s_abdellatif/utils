/**
 *
 */
package com.phi01tech.utils.model.mapper;

import java.lang.invoke.MethodHandles;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Collections;

/**
 * Default implementation for {@link ModelMapper}.
 * <p>
 * This implementation depends on a {@link PropertyNameExtractor}
 * strategy to extract the property names from model interfaces
 * when its methods are invoked and a {@link PropertyValueExtractor}
 * to extract property values from the model source. This
 * implementation is designed to be a fly-wieght and thread safe in
 * nature but it depends on the injected dependencies thread safty
 * and being fly-wieght.
 * </p>
 * <p>
 * This implementation uses Java Dynamic Proxy to create an implementation
 * of the target model interface and it has the option to eagerly populates
 * the returned instance, by default it returns a lazy model instance which
 * consumes the model source once requested.<br/>
 * </p>
 *
 * @author phi01tech
 */
public class DefaultModelMapper implements ModelMapper {
    // TODO add Java Beans style support by having each generated model implementing Map and
    // return the property names as the map keys

    private PropertyNameExtractor propertyNameExtractor = new GetterPropertyNameExtractor();
    private PropertyValueExtractor propertyValueExtractor = new DefaultPropertyValueExtractor();
    private ObjectMethodsHandler objectMethodsHandler = new DefaultObjectMethodsHandler();

    public void setPropertyNameExtractor(PropertyNameExtractor propertyNameExtractor) {
        this.propertyNameExtractor = propertyNameExtractor;
    }

    public void setPropertyValueExtractor(PropertyValueExtractor propertyValueExtractor) {
        this.propertyValueExtractor = propertyValueExtractor;
    }

    public void setObjectMethodsHandler(ObjectMethodsHandler objectMethodsHandler) {
        this.objectMethodsHandler = objectMethodsHandler;
    }

    public <T> T mapModel(Class<T> modelInterface) {
        return this.mapModel(Collections.emptyMap(), modelInterface);
    }

    @Override
    public <T> T mapModel(Object modelSource, Class<T> modelInterface) {
        validateModelAndTargeType(modelSource, modelInterface);
        return constructModelInstance(modelSource, modelInterface);
    }

    @SuppressWarnings("unchecked")
    private <T> T constructModelInstance(Object modelSource, Class<T> modelInterface) {
        LazyInvocationHandler<T> handler = new LazyInvocationHandler<>();
        handler.modelInterface = modelInterface;
        handler.modelSource = modelSource;

        return (T) Proxy.newProxyInstance(
                DefaultModelMapper.class.getClassLoader(),
                new Class[]{modelInterface},
                handler);
    }


    private <T> void validateModelAndTargeType(Object modelSource, Class<T> modelInterface) {
        if (modelSource == null)
            throw new IllegalArgumentException("Model map can't be null");
        if (modelInterface == null)
            throw new IllegalArgumentException("Model interface can't be null");
        if (!modelInterface.isInterface())
            throw new IllegalArgumentException("Model type shall be an interface");
    }

    private class LazyInvocationHandler<T> implements InvocationHandler {

        Class<T> modelInterface;
        Object modelSource;

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            MappingContext context = new MappingContextBean(DefaultModelMapper.this
                    , modelSource, modelInterface);
            MappingContext.setContext(context);
            try {
            	// TODO proper handling for isDefault
                if (method.getDeclaringClass().equals(Object.class))
                    return objectMethodsHandler.handle(proxy, method, args);
                String propertyName = propertyNameExtractor.extractPropertyName(modelInterface, method);
                Class<?> returnType = method.getReturnType();
                return propertyValueExtractor.extractPropertyValue(modelSource,
                        propertyName,
                        returnType);
            } finally {
                MappingContext.clearCurrent();
            }
        }
    }

}
