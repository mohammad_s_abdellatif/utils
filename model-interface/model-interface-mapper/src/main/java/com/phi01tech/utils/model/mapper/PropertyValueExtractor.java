/**
 *
 */
package com.phi01tech.utils.model.mapper;

/**
 * Defines a property value extraction strategy used by {@link DefaultModelMapper}
 * to extract values from a model source. An extractor would do conversion as well.
 *
 * @author phi01tech
 */
public interface PropertyValueExtractor {

    /**
     * Extract a property value with name <code>propertyName</code> from <code>modelSource</code>,
     * the returned value shall be of type <code>valueType</code>.
     * <p>
     * This method would also do conversion, like converting a String value from the model source to
     * an Integer value by parsing, converting a string value to a date, etc...
     * </p>
     *
     * @param modelSource  the source from where to extract property value
     * @param propertyName the property name
     * @param valueType    the target value type
     * @return property value with type <code>valueType</code>.
     */
    <T> T extractPropertyValue(Object modelSource, String propertyName, Class<? extends T> valueType);

}
