/**
 *
 */
package com.phi01tech.utils.model.mapper;

import java.lang.reflect.Method;

/**
 * @author phi01tech
 */
public class GetterPropertyNameExtractor implements PropertyNameExtractor {

    @Override
    public String extractPropertyName(Class<?> mappedType, Method method) {
        String methodName = method.getName();
        if (methodName.matches("get[A-Z]+\\w*")) {
            String propertyName = methodName.substring(3);
            if (propertyName.length() == 1)
                return propertyName.toLowerCase();
            return Character.toLowerCase(propertyName.charAt(0)) + propertyName.substring(1);
        }
        throw new NotPropertyReadMethodException("Method " + method.getName() + " is not a read property value method");
    }

}
