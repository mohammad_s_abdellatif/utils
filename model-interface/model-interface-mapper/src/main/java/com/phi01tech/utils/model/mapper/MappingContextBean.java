package com.phi01tech.utils.model.mapper;

/**
 * Created by phi01tech on 5/27/17.
 */
class MappingContextBean extends MappingContext {

    private ModelMapper modelMapper;

    private Object modelSource;

    private Class modelInterface;

    MappingContextBean(ModelMapper modelMapper, Object modelSource, Class modelInterface) {
        this.modelMapper = modelMapper;
        this.modelSource = modelSource;
        this.modelInterface = modelInterface;
    }

    @Override
    ModelMapper getModelMapper() {
        return modelMapper;
    }

    @Override
    Object getModelSource() {
        return modelSource;
    }

    @Override
    Class getModelInterface() {
        return modelInterface;
    }
}
