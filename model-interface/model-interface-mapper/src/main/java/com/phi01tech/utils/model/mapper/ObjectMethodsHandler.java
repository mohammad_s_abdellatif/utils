package com.phi01tech.utils.model.mapper;

import java.lang.reflect.Method;

/**
 * A strategy used to provide proper implementation
 * for Object methods for the generated model.
 * <p>
 * Created by phi01tech on 5/27/17.
 */
public interface ObjectMethodsHandler {

    Object handle(Object proxy, Method method, Object[] args) throws Exception;
}
