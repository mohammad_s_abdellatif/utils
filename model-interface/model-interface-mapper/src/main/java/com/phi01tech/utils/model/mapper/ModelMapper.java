package com.phi01tech.utils.model.mapper;

/**
 * Defines a model mapper which generates a model instance (normally
 * in a Java bean style) populated from a data source, i.e: Map,
 * Java Bean, HTTPServletRequest.
 * <p>
 * Usually you need such mapper to reduce the effort of implementing and
 * populating domain data interfaces transferred back and forth through data
 * access layer (like a Repository or a DAO) and reduces the needs for
 * converters and builders to force domain data encapsulation, below shows
 * a typical use case that reads parameters from an HTTP request, generates a
 * model from them, then pass the generated model to a DAO:
 * <p>
 * <pre>
 *     public interface Employee {
 *         int getId();
 *         String getFirstName();
 *         String getLastName();
 *     }
 *
 *     public void doPost(HttpServletRequest req, HttpServletResponse resp) {
 *         ModelMapper mapper = ....;// construct the mapper
 *         EmployeeDao dao = new EmployeeDao();
 *
 *         // ask the mapper to return an object implementing Employee interface
 *         // and populated with values sent as request parameters
 *         Employee employee = mapper.mapModel(req.getParameterMap(),Employee.class);
 *
 *         // Pass the employee instance to the dao for creation
 *         dao.add(employee);
 *     }
 * </pre>
 * <p>
 * Created by phi01tech on 5/26/17.
 */
public interface ModelMapper {

    /**
     * Generate a model instance implementing the <code>modelInterface</code>
     * populated with data from <code>modelSource</code>.
     * <p>
     * Model instances returned might be eagerly populated with property
     * values from the <code>modelSource</code>.
     * </p>
     * <p>
     * Implementation shall prepare an instance of {@link MappingContext}
     * before starting the process of mapping so the custom convertors and
     * extractors can get a reference for this invoked mapper mapping request.
     * </p>
     *
     * @param modelSource    the source of model data
     * @param modelInterface the target model interface
     * @param <T>            The interface type and returned model.
     * @return returned model populated from the model source.
     * @throws ModelMappingException if mapping is failed
     */
    <T> T mapModel(Object modelSource, Class<T> modelInterface) throws ModelMappingException;

}
