package com.phi01tech.utils.model.mapper;

/**
 * A base exception for model interface mapping.
 * <p>
 * Created by phi01tech on 5/27/17.
 */
public class ModelMappingException extends RuntimeException {

    public ModelMappingException() {
    }

    public ModelMappingException(String message) {
        super(message);
    }

    public ModelMappingException(String message, Throwable cause) {
        super(message, cause);
    }

    public ModelMappingException(Throwable cause) {
        super(cause);
    }

    public ModelMappingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
