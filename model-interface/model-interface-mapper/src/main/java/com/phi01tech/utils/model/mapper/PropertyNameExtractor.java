/**
 *
 */
package com.phi01tech.utils.model.mapper;

import java.lang.reflect.Method;

/**
 * Defines the strategy for extracting a property name (key) from the target model interface.
 *
 * @author Mohammad S. Abdellatif
 */
public interface PropertyNameExtractor {

    /**
     * Extract the property name (key) related to the target model interface
     * <code>mappedType</code> for model interface method <code>method</code>
     *
     * @param mappedType the target model type
     * @param method     the method invoked in the model instance
     * @return the property name (key)
     * @throws NotPropertyReadMethodException when the passed method is not a read property method
     */
    String extractPropertyName(Class<?> mappedType, Method method) throws NotPropertyReadMethodException;
}
