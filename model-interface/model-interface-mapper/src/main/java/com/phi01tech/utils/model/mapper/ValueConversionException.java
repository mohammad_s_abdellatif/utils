package com.phi01tech.utils.model.mapper;

/**
 * Created by phi01tech on 5/27/17.
 */
public class ValueConversionException extends ModelMappingException {



    public ValueConversionException(String message) {
        super(message);
    }

    public ValueConversionException(String message, Throwable cause) {
        super(message, cause);
    }

    public ValueConversionException(Throwable cause) {
        super(cause);
    }

    public ValueConversionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
