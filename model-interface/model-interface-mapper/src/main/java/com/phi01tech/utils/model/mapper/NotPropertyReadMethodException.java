package com.phi01tech.utils.model.mapper;

public class NotPropertyReadMethodException extends ModelMappingException {

    /**
     *
     */
    private static final long serialVersionUID = 5173408928639644698L;

    public NotPropertyReadMethodException(String message) {
        super(message);
    }
}
