package com.phi01tech.utils.model.mapper;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;

import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.beanutils.ConvertUtilsBean2;
import org.apache.commons.beanutils.Converter;

/**
 * A value convertor which utilizes
 * <a href="http://commons.apache.org/proper/commons-beanutils/">Apache
 * BeanUtils</a> <code>ConvertUtilsBean2</code> to do the value conversion.
 * <p>
 * Any customization can be done easly by passing a customized
 * {@link ConvertUtilsBean2} through constructor.
 * <p>
 * Created by phi01tech on 5/27/17.
 */
public class ConvertUtilsValueConverter implements ValueConverter {
	private final ConvertUtilsBean2 convertUtilsBean;
	private static final Converter ENUM_CONVERTOR = new Converter() {

		@Override
		public Object convert(Class type, Object value) {
			if (value instanceof String)
				return Enum.valueOf(type, (String) value);
			throw new ConversionException("unable to convert enum");
		}
	};

	public ConvertUtilsValueConverter() {
		this(new ConvertUtilsBean2());

		convertUtilsBean.register(new Converter() {

			@Override
			public Object convert(Class type, Object value) {
				if (value == null)
					return null;
				if (type == LocalDateTime.class) {
					if (value instanceof LocalDateTime)
						return value;
					if (value instanceof String)
						return LocalDateTime.parse((String) value);
					if (value instanceof Long)
						return Instant.ofEpochMilli((Long) value)
								.atOffset(ZoneOffset.UTC).toLocalDateTime();
					if (value instanceof Date)
						return Instant.ofEpochMilli(((Date) value).getTime())
								.atOffset(ZoneOffset.UTC).toLocalDateTime();
				}
				throw new ConversionException("unable to convert value to LocalDateTime: " + value);
			}
		}, LocalDateTime.class);

		convertUtilsBean.register(new Converter() {

			@Override
			public Object convert(Class type, Object value) {
				if (value == null)
					return null;
				if (type == LocalDate.class) {
					if (value instanceof LocalDate)
						return value;
					if (value instanceof String)
						return LocalDate.parse((String) value);
					if (value instanceof Date)
						return Instant.ofEpochMilli(((Date) value).getTime())
								.atOffset(ZoneOffset.UTC).toLocalDate();
				}
				throw new ConversionException("unable to convert value to LocalDate: " + value);
			}
		}, LocalDate.class);

	}

	public ConvertUtilsValueConverter(ConvertUtilsBean2 convertUtilsBean) {
		this.convertUtilsBean = convertUtilsBean;
	}

	@Override
	public boolean supports(Class<?> sourceType, Class<?> valueType) {
		return valueType.isEnum() || convertUtilsBean.lookup(sourceType, valueType) != null;
	}

	@Override
	public <T> T convert(Object value, Class<T> targetType) {
		if (value == null)
			return null;
		try {
			Converter converter = convertUtilsBean.lookup(value.getClass(), targetType);
			if (converter == null && targetType.isEnum())
				converter = ENUM_CONVERTOR;
			if (converter == null)
				throw new ValueConversionException(
						"Unable to convert value of type:  "
								+ value.getClass()
								+ " to type: "
								+ targetType
								+ ", try to register custom ValueConverter");

			return (T) converter.convert(targetType, value);
		} catch (ConversionException e) {
			throw new ValueConversionException(e);
		}
	}
}
