package com.phi01tech.utils.model.mapper;

/**
 * Defines a value converter strategy, used by {@link DefaultPropertyValueExtractor}
 * <p>
 * Created by phi01tech on 5/27/17.
 */
public interface ValueConverter {

    boolean supports(Class<?> sourceType, Class<?> valueType);

    /**
     * Convert <code>value</code> to the target type <code>valueType</code>.
     *
     * @param value     to convert
     * @param valueType target type
     * @param <T>       target type
     * @return converted value
     * @throws ValueConversionException if unable to convert the value
     */
    <T> T convert(Object value, Class<T> valueType) throws ValueConversionException;
}
