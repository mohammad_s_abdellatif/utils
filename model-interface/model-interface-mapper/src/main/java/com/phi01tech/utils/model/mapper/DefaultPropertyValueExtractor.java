/**
 *
 */
package com.phi01tech.utils.model.mapper;

import java.util.Map;

/**
 * A property value extractor which depends on a {@link ValueConverter} to
 * convert the values from model source to target property value.
 *
 * @author phi01tech
 */
public class DefaultPropertyValueExtractor implements PropertyValueExtractor {

	private final ValueConverter valueConverter;

	public DefaultPropertyValueExtractor() {
		this(new ConvertUtilsValueConverter());
	}

	public DefaultPropertyValueExtractor(ValueConverter valueConverter) {
		this.valueConverter = valueConverter;
	}

	@Override
	public <T> T extractPropertyValue(Object modelSource, String propertyName, Class<? extends T> valueType) {
		if (modelSource instanceof Map)
			return (T) extractPropertyValueFromMap((Map) modelSource, propertyName, valueType);
		throw new UnsupportedModelSourceException(modelSource.getClass().getCanonicalName() + " is not supported");
	}

	private Object extractPropertyValueFromMap(Map modelSource, String propertyName, Class<?> valueType) {
		if (!modelSource.containsKey(propertyName))
			return null;
		Object value = modelSource.get(propertyName);
		// move this to an embedded convertor
		if (value == null)
			// return the value as is
			return value;
		if (valueType == value.getClass())
			return value;
		// TODO move this to an embedded convertor
		if (valueType.isInterface() && value instanceof Map) {
			return embeddedMapConversion(valueType, value);

		}
		if (!valueConverter.supports(value.getClass(), valueType))
			throw new ValueConversionException("value convertor doesn't support conversion from: "
					+ value.getClass() + " to " + valueType);
		return valueConverter.convert(value, valueType);
	}

	private Object embeddedMapConversion(Class<?> modelType, Object source) {
		// embedded map conversion
		ModelMapper modelMapper = MappingContext.getContext().getModelMapper();
		MappingContext context = new MappingContextBean(modelMapper, source, modelType);
		MappingContext.embedContext(context);
		try {
			return modelMapper.mapModel(source, modelType);
		} finally {
			MappingContext.clearCurrent();
		}
	}

}
