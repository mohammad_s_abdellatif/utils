package com.phi01tech.utils.model.mapper;

import java.util.function.BiFunction;

/**
 * A {@link ValueConverter} implementation providing methods implementations
 * through Functional Interfaces.
 * <p>
 * Created by phi01tech on 5/30/17.
 */
public class FunctionalValueConverter implements ValueConverter {
    private BiFunction<Class, Class, Boolean> supports;
    private BiFunction<Object, Class, Object> convert;

    private FunctionalValueConverter(BiFunction<Class, Class, Boolean> supports, BiFunction<Object, Class, Object> convert) {
        this.supports = supports;
        this.convert = convert;
    }

    public static FunctionalValueConverter
    with(BiFunction<Class, Class, Boolean> supports, BiFunction<Object, Class, Object> convert) {
        return new FunctionalValueConverter(supports, convert);
    }

    @Override
    public boolean supports(Class<?> sourceType, Class<?> valueType) {
        return supports.apply(sourceType, valueType);
    }

    @Override
    public <T> T convert(Object value, Class<T> valueType) throws ValueConversionException {
        return (T) convert.apply(value, valueType);
    }
}
