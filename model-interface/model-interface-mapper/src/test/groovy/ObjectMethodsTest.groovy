import com.phi01tech.utils.model.mapper.DefaultModelMapper
import com.phi01tech.utils.model.mapper.ModelMapperBuilder
import com.phi01tech.utils.model.mapper.test.Point
import spock.lang.Specification

/**
 * Created by phi01tech on 5/27/17.
 */
class ObjectMethodsTest extends Specification {

    def "success when generate two models from same source then check equality to false"() {
        given:
        def mapper = new ModelMapperBuilder().build();
        def coordinates = [x: 10, y: 10];
        when:
        def model1 = mapper.mapModel(coordinates, Point)
        def model2 = mapper.mapModel(coordinates, Point)
        then:
        model1 != model2
        model1.hashCode() == model2.hashCode()
    }

    def "success when generate model and invoke equals method on its self"() {
        given:
        def mapper = new ModelMapperBuilder().build();
        def coordinates = [x: 10, y: 10];
        when:
        def model = mapper.mapModel(coordinates, Point)
        then:
        model == model
        model.hashCode() == model.hashCode()
    }
}
