import com.phi01tech.utils.model.mapper.DefaultModelMapper
import com.phi01tech.utils.model.mapper.ModelMapperBuilder
import com.phi01tech.utils.model.mapper.test.Employee
import spock.lang.Specification

/**
 * Created by phi01tech on 5/27/17.
 */
class EmbeddedModelTest extends Specification {

    def "success when generate embedded model"() {
        given:
        def mapper = new ModelMapperBuilder().build();
        def map = [
                firstName: "Mohammad",
                lastName : "Abdellatif",
                address  : [
                        city      : "Amman",
                        street    : "Arab Street",
                        buildingNo: 44
                ]] as HashMap;
        when:
        def model = mapper.mapModel(map, Employee)
        then:
        with(model) {
            firstName == "Mohammad"
            lastName == "Abdellatif"
            address != null
        }

        with(model.address) {
            city == "Amman"
            street == "Arab Street"
            buildingNo == 44
        }

    }



}