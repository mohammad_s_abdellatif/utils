import com.phi01tech.utils.model.mapper.DefaultModelMapper
import com.phi01tech.utils.model.mapper.ModelMapper
import com.phi01tech.utils.model.mapper.ModelMapperBuilder
import com.phi01tech.utils.model.mapper.test.Employee
import spock.lang.Specification

/**
 * Created by phi01tech on 5/26/17.
 */
class MapperTest extends Specification {
    def mapper = new ModelMapperBuilder().build() as ModelMapper

    def "fail when pass null map"() {
        when:
        mapper.mapModel(null, null)
        then:
        thrown(IllegalArgumentException)
    }

    def "fail when pass null type"() {
        given:
        def emptyMap = [] as Map;
        when:
        mapper.mapModel(emptyMap, null)
        then:
        thrown(IllegalArgumentException)
    }

    def "fail when pass non interface type"() {
        given:
        def emptyMap = [] as Map;
        when:
        mapper.mapModel(emptyMap, Object)
        then:
        thrown(IllegalArgumentException)
    }

    def "success when pass valid map with simple employee info"() {
        given:
        def properties = [
                "firstName": "Mohammad",
                "lastName" : "Abdellatif"
        ] as HashMap;
        when:
        def mapped = mapper.mapModel(properties, Employee)
        then:
        with(mapped) {
            firstName == "Mohammad"
            lastName == "Abdellatif"
        }
    }

}

