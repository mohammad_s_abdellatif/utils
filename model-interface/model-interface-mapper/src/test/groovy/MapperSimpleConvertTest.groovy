import com.phi01tech.utils.model.mapper.DefaultModelMapper
import com.phi01tech.utils.model.mapper.ModelMapper
import com.phi01tech.utils.model.mapper.ModelMapperBuilder
import com.phi01tech.utils.model.mapper.test.Location
import com.phi01tech.utils.model.mapper.test.Point
import com.phi01tech.utils.model.mapper.test.Resignation
import spock.lang.Specification

import java.text.SimpleDateFormat

/**
 * Created by phi01tech on 5/26/17.
 */
class MapperSimpleConvertTest extends Specification {
    //TODO test cases for primtivies shall be a java classes
    def mapper = new ModelMapperBuilder().build() as ModelMapper;

    def "success when pass null values"() {
        given:
        def coordinates = [x: null, y: null] as HashMap;
        when:
        def point = mapper.mapModel(coordinates, Point)
        then:
        assert point != null
        assert null == point.x
        assert point.y == null
    }

    def "success when pass simple simple coordinates object"() {
        given:
        def coordinates = ["x": "10", "y": "5"] as HashMap;
        when:
        def point = mapper.mapModel(coordinates, Point)
        then:
        with(point) {
            x == 10
            y == 5
        }
    }

    def "success when pass simple map coordinate object"() {
        given:
        def coordinates = [latitude: "32.0437062", longitude: "35.9097593"] as HashMap;
        when:
        def point = mapper.mapModel(coordinates, Location)
        then:
        with(point) {
            latitude == 32.0437062
            longitude == 35.9097593
        }
    }

    def "success when pass date fields"() {
        given:
        def now = System.currentTimeMillis()
        def resignation = [date: now, cause: "Cut cost"] as HashMap
        when:
        def model = mapper.mapModel(resignation, Resignation)
        then:
        with(model) {
            date.getTime() == now
            cause == "Cut cost"
        }
    }

    def "success when return null properties from source"() {
        given:
        def source = [] as HashMap
        when:
        def model = mapper.mapModel(source, Resignation)
        then:
        assert model
        assert !model.date
    }
}