package com.phi01tech.utils.model.mapper.test;

import com.phi01tech.utils.model.mapper.ModelMapper;
import com.phi01tech.utils.model.mapper.ModelMapperBuilder;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by phi01tech on 5/30/17.
 */
public class CollectionsMapping {

    @Test
    public void successWhenMapListToArray() {
        ModelMapperBuilder builder = new ModelMapperBuilder();
        ModelMapper modelMapper = builder.build();
        Map<String, Object> source = new HashMap<>();

        source.put("hobbies", Arrays.asList("swimming", "reading"));
        Employee employee = modelMapper.mapModel(source, Employee.class);
        assertNotNull(employee);
        assertArrayEquals(new String[]{"swimming", "reading"}, employee.getHobbies());
    }

    @Test
    public void successWhenMapSetToArray() {
        ModelMapperBuilder builder = new ModelMapperBuilder();
        ModelMapper modelMapper = builder.build();
        Map<String, Object> source = new HashMap<>();

        source.put("hobbies", new HashSet<>(Arrays.asList("swimming", "reading", "swimming")));
        Employee employee = modelMapper.mapModel(source, Employee.class);
        assertNotNull(employee);
        assertArrayEquals(new String[]{"swimming", "reading"}, employee.getHobbies());
    }

    @Test
    @Ignore
    public void successWhenMapArrayToList() {
        ModelMapperBuilder builder = new ModelMapperBuilder();
        ModelMapper modelMapper = builder.build();
        Map<String, Object> source = new HashMap<>();

        source.put("searchTerms", new String[]{"java", "pattern"});
        Terms term = modelMapper.mapModel(source, Terms.class);
        assertNotNull(term);
        assertEquals(Arrays.asList("java", "pattern"), term.getSearchTerms());
    }
}
