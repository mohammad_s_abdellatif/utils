package com.phi01tech.utils.model.mapper.test;

public interface Location {
    Double getLatitude();

    Double getLongitude();
}