package com.phi01tech.utils.model.mapper.test;

public interface Address {
    String getCity();

    String getStreet();

    int getBuildingNo();
}