package com.phi01tech.utils.model.mapper.test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by phi01tech on 5/30/17.
 */
public interface Terms {

    ArrayList<String> getSearchTerms();
}
