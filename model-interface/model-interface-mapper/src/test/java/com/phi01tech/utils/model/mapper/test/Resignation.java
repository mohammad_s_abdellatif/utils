package com.phi01tech.utils.model.mapper.test;

import java.util.Date;

public interface Resignation {
    Date getDate();

    String getCause();
}