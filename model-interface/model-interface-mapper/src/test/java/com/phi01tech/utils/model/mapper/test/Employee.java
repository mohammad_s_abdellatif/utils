package com.phi01tech.utils.model.mapper.test;

public interface Employee {
    String getFirstName();

    String getLastName();

    Address getAddress();

    String[] getHobbies();
}
