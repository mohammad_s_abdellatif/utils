package com.phi01tech.utils.model.mapper.test;

import com.phi01tech.utils.model.mapper.ModelMapper;
import com.phi01tech.utils.model.mapper.ModelMapperBuilder;
import com.phi01tech.utils.model.mapper.ValueConversionException;
import org.junit.Test;

import java.util.HashMap;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by phi01tech on 5/30/17.
 */
public class ArrayMappingTest {

    @Test
    public void successWhenPassArrayForSingleValue() {
        ModelMapperBuilder builder = new ModelMapperBuilder();
        ModelMapper modelMapper = builder.build();
        HashMap<Object, Object> map = new HashMap<>();

        map.put("firstName", new String[]{"Mohammad", "Sami"});
        map.put("lastName", "Abdellatif");
        String[] hobbies = {"Swimming"};
        map.put("hobbies", "Swimming");
        Employee employee = modelMapper.mapModel(map, Employee.class);
        assertNotNull(employee);
        assertEquals("Mohammad", employee.getFirstName());
        assertEquals("Abdellatif", employee.getLastName());
        assertArrayEquals(hobbies, employee.getHobbies());
    }
}
