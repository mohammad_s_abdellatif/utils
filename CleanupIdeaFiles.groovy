import groovy.io.FileType

/**
 * Created by phi01tech on 5/5/17.
 */
def currentPath = new File("./")
println "Iterating through path: $currentPath.canonicalPath"
deleteImlFilesInPath(currentPath)
def dirToDelete = [];
currentPath.eachDirRecurse { f ->
    def name = f.name
    if (f.exists() && (name == ".idea" || name == ".settings")) {
        println "found idea directory $f.canonicalPath"
	dirToDelete.add(f);
    } else {
        deleteImlFilesInPath(f)
    }
}

for(f in dirToDelete) {
   println "delete dir $f.canonicalFile"
   f.canonicalFile.deleteDir()
}

def deleteImlFilesInPath(File f) {
    if(f.exists())
	    f.eachFile {
		if (it.name.endsWith(".iml") || it.name == '.classpath' || it.name == '.project') {
		    println "Delete file: $it.name"
		    it.delete();
		}
	    }
}
