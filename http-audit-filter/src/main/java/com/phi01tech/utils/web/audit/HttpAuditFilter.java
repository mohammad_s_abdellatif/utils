package com.phi01tech.utils.web.audit;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.function.Consumer;
import java.util.function.Predicate;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HttpAuditFilter implements Filter {
	private static final String LOGGER_NAME = "HTTP-AUDIT-FILTER";

	private static final Predicate<HttpServletRequest> FILTER_ALL_REQUESTS = (r) -> true;

	private static Logger logger = LoggerFactory.getLogger(LOGGER_NAME);
	private Predicate<HttpServletRequest> filterPredicate = FILTER_ALL_REQUESTS;
	private Consumer<HttpAuditLog> auditLogger = this::logAudit;

	public HttpAuditFilter() {

	}

	public HttpAuditFilter(
			Predicate<HttpServletRequest> filterPredicate,
			Consumer<HttpAuditLog> auditLogger) {
		this.filterPredicate = filterPredicate;
		this.auditLogger = auditLogger;
	}

	public void setFilterPredicate(Predicate<HttpServletRequest> filterPredicate) {
		this.filterPredicate = filterPredicate;
	}

	public void setAuditLogger(Consumer<HttpAuditLog> auditLogger) {
		this.auditLogger = auditLogger;
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		logger.info("Audit filter initialized");
		this.filterPredicate = instantiateDependencyFromParam(filterConfig, "predicate.class", this.filterPredicate);
		this.auditLogger = instantiateDependencyFromParam(filterConfig, "consumer.class", this.auditLogger);

		if (this.filterPredicate == null)
			throw new ServletException(
					"No predicate was specified, the predicate.class init parameter was not specified or not injected");

		if (this.auditLogger == null)
			throw new ServletException(
					"No audit log consumer was specified, the auditLogger.class init parameter was not specified or not injected");

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpReq = (HttpServletRequest) request;
		HttpServletResponse httpResp = (HttpServletResponse) response;
		if (!filterPredicate.test(httpReq)) {
			chain.doFilter(request, response);
			return;
		}
		long start = System.currentTimeMillis();
		AuditHttpServletRequest reqWrapper = new AuditHttpServletRequest(httpReq);
		AuditHttpServletResponse respWrapper = new AuditHttpServletResponse(httpResp);

		chain.doFilter(reqWrapper, respWrapper);
		long end = System.currentTimeMillis();
		HttpAuditLog auditLog = constructAuditLog(reqWrapper, respWrapper, start, end);
		this.auditLogger.accept(auditLog);
	}

	protected HttpAuditLog constructAuditLog(AuditHttpServletRequest reqWrapper, AuditHttpServletResponse respWrapper,
			long start, long end) {
		HttpAuditLog auditLog = new HttpAuditLog() {

			@Override
			public byte[] getResponseBody() {
				return respWrapper.getResponseBytes();
			}

			@Override
			public byte[] getRequestBody() {
				return reqWrapper.getRequestBody();
			}

			@Override
			public HttpServletResponse getHttpServletResponse() {
				return respWrapper;
			}

			@Override
			public HttpServletRequest getHttpServletRequest() {
				return reqWrapper;
			}

			@Override
			public long requestStartTimestamp() {
				return start;
			}

			@Override
			public long requestEndTimestamp() {
				return end;
			}
		};
		return auditLog;
	}

	@Override
	public void destroy() {
		logger.info("Audit filter destroyed");
	}

	protected void logAudit(HttpAuditLog auditLog) {
		HttpServletRequest request = auditLog.getHttpServletRequest();
		HttpServletResponse response = auditLog.getHttpServletResponse();
		String method = request.getMethod();
		String uri = request.getRequestURI();
		int status = response.getStatus();
		String contentType = response.getContentType();
		StringWriter out = new StringWriter();
		PrintWriter writer = new PrintWriter(out);
		writer.println();
		writer.append(method).append(" ").println(uri);
		writer.append("status: ").println(status);
		writer.append("content type: ").println(contentType);
		writer.append("took: ").println(auditLog.requestTotalTime());

		logger.info(out.toString());
	}

	@SuppressWarnings("unchecked")
	protected <T> T instantiateDependencyFromParam(FilterConfig filterConfig, String paramClassName, T defaultObj)
			throws ServletException {
		String predicateClass = filterConfig.getInitParameter(paramClassName);
		logger.info(paramClassName + " init param: " + predicateClass);
		if (predicateClass == null) {
			return defaultObj;
		}
		return (T) instantiateDependency(predicateClass);
	}

	protected Object instantiateDependency(String predicateClass) throws ServletException {
		try {
			return Class.forName(predicateClass).newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			throw new ServletException("unable to initialize HttpAuditFilter predicate", e);
		}
	}

}
