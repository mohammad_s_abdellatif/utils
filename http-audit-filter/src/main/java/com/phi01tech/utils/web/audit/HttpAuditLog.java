package com.phi01tech.utils.web.audit;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface HttpAuditLog {

	long requestStartTimestamp();

	long requestEndTimestamp();

	byte[] getRequestBody();

	byte[] getResponseBody();

	HttpServletRequest getHttpServletRequest();

	HttpServletResponse getHttpServletResponse();

	default String getRequestBodyAsString(String charset) {
		try {
			return new String(getRequestBody(), charset);
		} catch (UnsupportedEncodingException e) {
			throw new IllegalArgumentException(e);
		}
	}

	default String getResponseBodyAsString(String charset) {
		try {
			return new String(getResponseBody(), charset);
		} catch (UnsupportedEncodingException e) {
			throw new IllegalArgumentException(e);
		}
	}

	default long requestTotalTime() {
		return requestEndTimestamp() - requestStartTimestamp();
	}
}
