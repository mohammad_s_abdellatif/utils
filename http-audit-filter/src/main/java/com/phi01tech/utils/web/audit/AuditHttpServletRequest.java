package com.phi01tech.utils.web.audit;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class AuditHttpServletRequest extends HttpServletRequestWrapper {

	private final LinkedList<Byte> buffer = new LinkedList<>();

	public AuditHttpServletRequest(HttpServletRequest request) {
		super(request);
	}

	@Override
	public ServletInputStream getInputStream() throws IOException {
		ServletInputStream orgInput = super.getInputStream();
		return new ServletInputStream() {

			@Override
			public int read() throws IOException {
				int read = orgInput.read();
				if (read != -1)
					buffer.add((byte) read);
				return read;
			}

			@Override
			public void setReadListener(ReadListener readListener) {
				orgInput.setReadListener(readListener);
			}

			@Override
			public boolean isReady() {
				return orgInput.isReady();
			}

			@Override
			public boolean isFinished() {
				return orgInput.isFinished();
			}
		};
	}

	public byte[] getRequestBody() {
		byte[] arr = new byte[buffer.size()];
		int count = 0;
		for (Byte b : buffer) {
			arr[count++] = b;
		}
		return arr;
	}

}
