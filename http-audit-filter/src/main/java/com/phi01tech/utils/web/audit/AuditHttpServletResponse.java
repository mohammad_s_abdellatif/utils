package com.phi01tech.utils.web.audit;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

public class AuditHttpServletResponse extends HttpServletResponseWrapper {
	private LinkedList<Byte> bytes = new LinkedList<>();

	public AuditHttpServletResponse(HttpServletResponse response) {
		super(response);
	}

	@Override
	public ServletOutputStream getOutputStream() throws IOException {
		ServletOutputStream outputStream = super.getOutputStream();
		return new ServletOutputStream() {

			@Override
			public void write(int b) throws IOException {
				outputStream.write(b);
				bytes.add((byte) b);
			}

			@Override
			public void setWriteListener(WriteListener writeListener) {
				outputStream.setWriteListener(writeListener);
			}

			@Override
			public boolean isReady() {
				return outputStream.isReady();
			}
		};
	}

	public byte[] getResponseBytes() {
		byte[] arr = new byte[bytes.size()];
		int count = 0;
		for (byte b : bytes) {
			arr[count++] = b;
		}
		return arr;
	}

}
