/**
 * 
 */
package com.phi01techl.spi;

import java.util.Collections;

/**
 * Defines implementer as a &quot;Service Provider Interface&quot;
 * 
 * @author Mohammad S. Abdellatif
 *
 */
public interface Spi {

	/**
	 * Returns the name of this SPI.
	 * 
	 * @return
	 */
	String getName();

	/**
	 * Returns the group (mostly the company or open source group) for this SPI.
	 * 
	 * @return
	 */
	String getGroupName();

	/**
	 * Returns its version.
	 * 
	 * @return
	 */
	Version getVersion();

	/**
	 * Return SPI version as string.
	 * 
	 * @return
	 */
	default String getVersionAsString() {
		Version version = getVersion();
		return version.getMajorVersion() + "." + version.getMinorVersion() + "." + version.getBuilderNo();
	}

	/**
	 * @return
	 */
	default Iterable<Feature> getFeatures() {
		return Collections.emptyList();
	}

}
