/**
 * 
 */
package com.phi01techl.spi;

/**
 * @author phi01tech
 *
 */
public interface Feature {

	/**
	 * @return
	 */
	String getName();

	/**
	 * @return
	 */
	default String getDescription() {
		return "";
	}
}
