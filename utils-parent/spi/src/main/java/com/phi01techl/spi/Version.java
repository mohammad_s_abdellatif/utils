package com.phi01techl.spi;

public interface Version {

	int getMajorVersion();

	int getMinorVersion();

	default int getBuilderNo() {
		return 0;
	}

}
