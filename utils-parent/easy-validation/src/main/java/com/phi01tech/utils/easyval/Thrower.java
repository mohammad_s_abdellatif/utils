package com.phi01tech.utils.easyval;

public interface Thrower {

	void throwValidationException(String message);
}
