package com.phi01tech.utils.easyval;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator<T> {

	private T input;
	private Thrower thrower = new DefaultThrower();
	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
			Pattern.CASE_INSENSITIVE);

	private Validator(T input) {
		this.input = input;
	}
	
	public T getInput() {
		return input;
	}

	public static <T> Validator<T> beanValidator(T input) {
		return new Validator<T>(input);
	}

	public Validator<T> userDefaultThrower() {
		this.thrower = new DefaultThrower();
		return this;
	}

	public Validator<T> useThrower(Thrower thrower) {
		if (thrower == null)
			throw new IllegalArgumentException("Thrower can't be null");
		this.thrower = thrower;
		return this;
	}

	public Validator<T> notNull() {
		return notNull("input is null");
	}

	public Validator<T> notNull(String message) {
		if (input == null)
			thrower.throwValidationException(message);
		return this;
	}

	public <R> Validator<T> notNull(Function<? super T, R> extractor) {
		return notNull(extractor, "value is null");
	}

	public <R> Validator<T> notNull(Function<? super T, R> extractor, String message) {
		R r = extractor.apply(input);
		if (r == null)
			thrower.throwValidationException(message);
		return this;
	}

	public Validator<T> notEmpty(Function<? super T, String> extractor) {
		return notEmpty(extractor, "string is empty");
	}

	public Validator<T> notEmpty(Function<? super T, String> extractor, String message) {
		String r = extractor.apply(input);
		if (r == null || r.trim().equals(""))
			thrower.throwValidationException(message);
		return this;
	}

	public <R> Validator<T> notEmptyArray(Function<? super T, R[]> extractor) {
		return notEmptyArray(extractor, "array is empty");
	}

	public <R> Validator<T> notEmptyArray(Function<? super T, R[]> extractor, String message) {
		R[] r = extractor.apply(input);
		if (r == null || r.length == 0)
			thrower.throwValidationException(message);
		return this;
	}

	public Validator<T> notEmptyArrayValues(Function<? super T, String[]> extractor, String message) {
		String[] r = extractor.apply(input);
		if (r.length == 0)
			thrower.throwValidationException(message);
		for (String s : r)
			notEmpty((i) -> s, message);
		return this;
	}

	public Validator<T> notEmptyArrayValues(Function<? super T, String[]> extractor) {
		return notEmptyArrayValues(extractor, "one of array values is empty");
	}

	public Validator<T> isEmail(Function<? super T, String> extractor) {
		return isEmail(extractor, "not an email");
	}

	public Validator<T> isEmail(Function<? super T, String> extractor, String message) {
		Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(extractor.apply(input));
		if (!matcher.matches())
			thrower.throwValidationException(message);
		return this;
	}

	public Validator<T> lengthGte(Function<? super T, String> extractor, int length) {
		return lengthGte(extractor, length, "string length is greater than " + length);
	}

	public Validator<T> lengthGte(Function<? super T, String> extractor, int length, String message) {
		if (extractor.apply(input).length() < length)
			thrower.throwValidationException(message);
		return this;
	}

	public Validator<T> equals(Function<? super T, ? extends Object> extractor, Object value) {
		return equals(extractor, value, "value is not as expected");
	}

	public Validator<T> equals(Function<? super T, ? extends Object> extractor, Object value, String message) {
		if (!extractor.apply(input).equals(value))
			thrower.throwValidationException(message);
		return this;
	}

	public Validator<T> match(Predicate<? super T> predicate) {
		return match(predicate, "Condition not matched");
	}

	public Validator<T> match(Predicate<? super T> predicate, String message) {
		if (!predicate.test(input))
			thrower.throwValidationException(message);
		return this;
	}

	public Validator<T> notInFuture(Function<? super T, LocalDate> extractor) {
		return notInFuture(extractor, "date can't be in future");
	}

	public Validator<T> notInFuture(Function<? super T, LocalDate> extractor, String message) {
		LocalDate date = extractor.apply(input);
		if (date == null || LocalDate.now().isBefore(date))
			thrower.throwValidationException(message);
		return this;
	}

	public Validator<T> notInPast(Function<? super T, LocalDateTime> extractor) {
		return notInPast(extractor, "date can't be in future");
	}

	public Validator<T> notInPast(Function<? super T, LocalDateTime> extractor, String message) {
		LocalDateTime date = extractor.apply(input);
		if (date == null || LocalDateTime.now().isAfter(date))
			thrower.throwValidationException(message);
		return this;
	}
}
