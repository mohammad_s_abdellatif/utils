package com.phi01tech.utils.easyval;

public class DefaultThrower implements Thrower {

	@Override
	public void throwValidationException(String message) {
		throw new InvalidInputException(message);
	}

}
