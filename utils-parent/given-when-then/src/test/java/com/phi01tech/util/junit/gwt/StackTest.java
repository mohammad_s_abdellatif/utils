package com.phi01tech.util.junit.gwt;
import static org.junit.Assert.*;
import org.junit.Test;

import com.phi01tech.util.junit.gwt.Stack;
import com.phi01tech.util.junit.gwt.GivenWhenThenTest;

public class StackTest extends GivenWhenThenTest {

	@Test
	public void createDefaultSizeStack() {
		given(this::defaultStack)
				.when(NOTHING)
				.then((i) -> assertEquals("Default stack size", Stack.DEFAULT_SIZE, i.capacity()));
	}

	@Test
	public void createStackWithSpecificSize() {
		given(this::stackWithSizeFive)
				.when(NOTHING)
				.then((s) -> assertEquals("Stack with size 5", 5, s.capacity()));
	}

	@Test
	public void stackSizeIsOne() {
		given(this::defaultStack)
				.when(this::pushFirst)
				.then((s) -> assertEquals("Stack with size 1", 1, s.size()));
	}

	@Test
	public void stackIsFull() {
		given(this::stackWithSizeOne)
				.when(this::pushFirst).and(this::pushSecond)
				.thenThrowException(IllegalStateException.class);
	}

	@Test
	public void popLastElementPushed() {
		given(this::defaultStack)
				.when(this::pushFirst).and(this::pushSecond)
				.then((s) -> assertEquals("second", s.pop()));
	}

	@Test
	public void stackIsEmpty() {
		given(this::defaultStack)
				.when(this::popValue)
				.thenThrowException(IllegalStateException.class);
	}

	private Object popValue(Stack<Object> s) {
		return s.pop();
	}

	private Stack<Object> stackWithSizeOne() {
		return new Stack<>(1);
	}

	private Stack<Object> stackWithSizeFive() {
		return new Stack<>(5);
	}

	private Stack<Object> defaultStack() {
		return new Stack<>();
	}

	private void pushFirst(Stack<Object> s) {
		s.push("first");
	}

	private void pushSecond(Stack<Object> s) {
		s.push("second");
	}

}
