package com.phi01tech.util.junit.gwt;

public class CharacterArrayUtility {

	public static char[] reverse(char[] a) {
		char[] result = new char[a.length];
		for (int i = 0; i < a.length; i++)
			result[a.length - i - 1] = a[i];
		return result;
	}

}
