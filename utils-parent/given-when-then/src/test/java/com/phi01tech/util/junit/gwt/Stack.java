package com.phi01tech.util.junit.gwt;

public class Stack<T> {

	private final Object[] elements;
	public static final int DEFAULT_SIZE = 10;
	private int top = 0;

	public Stack() {
		elements = new Object[DEFAULT_SIZE];
	}

	public Stack(int size) {
		elements = new Object[size];
	}

	public int capacity() {
		return elements.length;
	}

	public int size() {
		return top;
	}

	public void push(T value) {
		if (top == elements.length)
			throw new IllegalStateException("Stack is full");
		elements[top++] = value;
	}

	public T pop() {
		if (top == 0)
			throw new IllegalStateException("Stack is empty");
		return (T) elements[--top];
	}
}
