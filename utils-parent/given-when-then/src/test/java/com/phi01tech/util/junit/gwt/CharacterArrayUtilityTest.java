package com.phi01tech.util.junit.gwt;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CharacterArrayUtilityTest extends GivenWhenThenTest {

	private char[] arrayToTest;

	@Before
	public void before() {
		arrayToTest = new char[] { 'A', 'B', 'C' };
	}

	private char[] arrayToTest() {
		return arrayToTest;
	}

	@Test
	public void reverseCharArray() {
		given(this::arrayToTest)
				.whenReturningResult(CharacterArrayUtility::reverse)
				.then((i, r) -> {
					Assert.assertArrayEquals(r, new char[] { 'C', 'B', 'A' });
				});
	}
}
