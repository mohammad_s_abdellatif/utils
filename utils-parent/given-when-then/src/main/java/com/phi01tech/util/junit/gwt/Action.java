/**
 * 
 */
package com.phi01tech.util.junit.gwt;

/**
 * @author phi01tech
 *
 */
public interface Action {

	void execute();
}
