/**
 * 
 */
package com.phi01tech.utils.db.importer;

/**
 * @author phi01tech
 *
 */
public class DataImporterException extends RuntimeException {

	/**
	 * 
	 */
	public DataImporterException() {
	}

	/**
	 * @param message
	 */
	public DataImporterException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public DataImporterException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public DataImporterException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public DataImporterException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
