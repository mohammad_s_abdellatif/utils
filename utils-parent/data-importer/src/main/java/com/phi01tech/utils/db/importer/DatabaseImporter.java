/**
 * 
 */
package com.phi01tech.utils.db.importer;

/**
 * @author phi01tech
 *
 */
public interface DatabaseImporter {

	public void createDatabase(String dbName) throws DataImporterException;

	public void dropDatabase(String dbName) throws DataImporterException;

	public void importInitialData(String dbName) throws DataImporterException;

}
