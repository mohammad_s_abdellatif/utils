package com.phi01tech.db.importer.mongo;

import java.io.IOException;
import java.io.InputStream;

public interface ScriptsLoader {

	void loadScripts(String resourceName, StreamConsumer resourceConsumer) throws IOException;

	interface StreamConsumer {
		void apply(InputStream is) throws IOException;
	}
}
