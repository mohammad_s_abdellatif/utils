package com.phi01tech.db.importer.mongo;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import com.phi01tech.utils.db.importer.DataImporterException;
import com.phi01tech.utils.db.importer.DatabaseImporter;

public class MongoDBDataImporter implements DatabaseImporter {
	private final MongoClient mongoClient;
	private final List<String> scriptsToInclude;
	private ScriptsLoader scriptsLoader = new ClasspathScriptsLoader();

	public MongoDBDataImporter(MongoClient mongoClient) {
		this(mongoClient, new ArrayList<>());
	}

	public MongoDBDataImporter(MongoClient mongoClient, List<String> scriptsToInclude) {
		this.mongoClient = mongoClient;
		this.scriptsToInclude = scriptsToInclude;
	}

	public void setScriptsLoader(ScriptsLoader scriptsLoader) {
		this.scriptsLoader = scriptsLoader;
	}

	public void setScriptsPathToInclude(String... scriptsPath) {
		scriptsToInclude.addAll(Arrays.asList(scriptsPath));
	}

	public void createDatabase(String dbName) {
		mongoClient.getDatabase(dbName);
	}

	public void dropDatabase(String dbName) {
		mongoClient.dropDatabase(dbName);
	}

	public void importInitialData(String dbName) {
		MongoDatabase database = mongoClient.getDatabase(dbName);
		runResourcesScripts(database);
	}

	protected void runResourcesScripts(MongoDatabase db) {
		try {
			for (String resource : scriptsToInclude)
				lookupThenExecute(db, resource);
		} catch (IOException e) {
			throw new DataImporterException("unable to load resources", e);
		}
	}

	private void lookupThenExecute(MongoDatabase db, String resource) throws IOException {
		scriptsLoader.loadScripts(resource, (is) -> {
			executeScript(db, is);
		});
	}

	private void executeScript(MongoDatabase db, InputStream stream) throws IOException {
		String content = readScriptContent(stream);
		executeCommand(db, content);
	}

	protected void executeCommand(MongoDatabase db, String content) {
		BasicDBObject command = new BasicDBObject();

		command.put("eval", "function(){" + content + "}");
		db.runCommand(command);
	}

	protected String readScriptContent(InputStream stream) throws IOException {
		Reader reader = new InputStreamReader(stream);
		StringBuilder scriptContent = new StringBuilder();
		int length = 0;

		char[] cbuf = new char[100];
		while ((length = reader.read(cbuf)) > 0)
			scriptContent.append(cbuf, 0, length);

		String content = scriptContent.toString();
		return content;
	}
}
