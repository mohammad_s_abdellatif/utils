package com.phi01tech.db.importer.mongo;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;

public class ClasspathScriptsLoader implements ScriptsLoader {

	@Override
	public void loadScripts(String resourceName, StreamConsumer resourceConsumer) throws IOException {
		ClassLoader classLoader = this.getClass().getClassLoader();
		Enumeration<URL> resources = classLoader.getResources(resourceName);
		while (resources.hasMoreElements()) {
			URL url = (URL) resources.nextElement();
			try (InputStream is = url.openStream()) {
				resourceConsumer.apply(is);
			}
		}
	}

}
