/**
 * 
 */
package com.phi01tech.utils.repository.mongodb;

/**
 * @author phi01tech
 *
 */
public class RecordNotFoundException extends MongoRepositoryException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7347002219278357887L;

	/**
	 * 
	 */
	public RecordNotFoundException() {
	}

	/**
	 * @param message
	 */
	public RecordNotFoundException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public RecordNotFoundException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public RecordNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public RecordNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
