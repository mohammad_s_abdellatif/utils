/**
 * 
 */
package com.phi01tech.utils.repository.mongodb;

/**
 * @author phi01tech
 *
 */
public class MongoRepositoryException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9075605912342334314L;

	/**
	 * 
	 */
	public MongoRepositoryException() {
	}

	/**
	 * @param message
	 */
	public MongoRepositoryException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public MongoRepositoryException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public MongoRepositoryException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public MongoRepositoryException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
