/**
 * 
 */
package com.phi01tech.utils.repository.mongodb;

/**
 * @author phi01tech
 *
 */
public class MultipleMatchesFoundException extends MongoRepositoryException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6765054619990986417L;

	/**
	 * 
	 */
	public MultipleMatchesFoundException() {
	}

	/**
	 * @param message
	 */
	public MultipleMatchesFoundException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public MultipleMatchesFoundException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public MultipleMatchesFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public MultipleMatchesFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
