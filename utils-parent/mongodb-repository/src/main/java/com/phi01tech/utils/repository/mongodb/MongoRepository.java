package com.phi01tech.utils.repository.mongodb;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import com.mongodb.CursorType;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;

public class MongoRepository {

	private MongoDatabase mongoDatabase;
	private String collectionName;
	protected Mappers mappers;

	public MongoRepository(MongoDatabase mongoDatabase, String collectionName, Mappers mappers) {
		this.mongoDatabase = mongoDatabase;
		this.collectionName = collectionName;
		this.mappers = mappers;
	}

	public MongoRepository(MongoDatabase mongoDatabase, String collectionName) {
		this(mongoDatabase, collectionName, Mappers.NOP);
	}

	public void setMappers(Mappers mappers) {
		this.mappers = mappers;
	}

	protected MongoCollection<Document> getCollection() {
		return this.mongoDatabase.getCollection(collectionName);
	}

	public final String getCollectionName() {
		return collectionName;
	}

	public Document getDocumentById(String docId) {
		MongoCollection<Document> collection = getCollection();
		Document document = collection.find(Filters.eq("_id", new ObjectId(docId))).first();
		if (document == null)
			throw new RecordNotFoundException("No record found with id: " + docId);
		return document;
	}

	public boolean countOneById(String id) {
		return countOneByFilter(Filters.eq("_id", new ObjectId(id)));
	}

	public boolean countOneByFilter(Bson filter) {
		long count = getCollection().count(filter);
		if (count > 1)
			throw new MultipleMatchesFoundException("Multiple matches found");
		return count == 1;
	}

	public Document findOneByFilter(Bson filter) {
		boolean found = countOneByFilter(filter);
		if (!found)
			throw new RecordNotFoundException("No single document found for filter");
		return getCollection().find(filter).first();
	}

	public <T> T findOneByFilterThenMap(Bson filter, Class<T> type) {
		DocToBeanMapper<T> mapper = mappers.getDocToBeanMapper(type);
		return (T) this.findOneByFilterThenMap(filter, mapper::fromDocument);
	}

	public <T> T findOneByFilterThenMap(Bson filter, Function<Document, T> mapper) {
		boolean found = countOneByFilter(filter);
		if (!found)
			throw new RecordNotFoundException("No single document found for filter");
		return mapper.apply(getCollection().find(filter).first());
	}

	public <T> T findOneThenMap(String docId, Function<Document, T> mapper) {
		Document doc = getDocumentById(docId);
		return mapper.apply(doc);
	}

	public void updateExistingDocByFilter(Bson filter, Bson updates) {
		Document doc = getCollection().findOneAndUpdate(filter, updates);
		if (doc == null)
			throw new RecordNotFoundException("No record to update");
	}

	public void updateExistingDocById(String id, Bson updates) {
		Bson filter = Filters.eq("_id", new ObjectId(id));
		updateExistingDocByFilter(filter, updates);
	}

	public <T> Document mapBeanToDoc(T bean) {
		@SuppressWarnings("unchecked")
		BeanToDocMapper<T> mapper = mappers.getBeanToDocMapper((Class<T>) bean.getClass());
		return mapper.fromBean(bean);
	}

	public Document add(Document doc) {
		if (doc == null)
			throw new IllegalArgumentException("Document can't be null");
		getCollection().insertOne(doc);
		return doc;
	}

	public <T> Document mapThenAdd(T bean) {
		if (bean == null)
			throw new IllegalArgumentException("Bean can't be null");
		BeanToDocMapper mapper = mappers.getBeanToDocMapper(bean.getClass());
		return add(mapper.fromBean(bean));
	}

	public FindIterable<Document> executeQuery(Bson filters, Bson projections) {
		return executeQuery(filters, (r) -> r.projection(projections));
	}

	public long countByFilter(Bson filter) {
		return getCollection().count(filter);
	}
	
	public FindIterable<Document> executeQuery(Bson filters,
			Function<FindIterable<Document>, FindIterable<Document>> queryCustomizer) {
		FindIterable<Document> result = getCollection()
				.find(filters);
		return queryCustomizer.apply(result);
	}

	public FindIterable<Document> executeQuery(Bson filters) {
		return getCollection().find(filters);
	}

	protected <T> Iterable<T> findByFilterThenMap(Bson filter, Class<T> type) {
		FindIterable<Document> finder = getCollection().find(filter);
		MongoCursor<Document> docs = finder.iterator();
		List<T> beans = new ArrayList<>();
		DocToBeanMapper<T> docToBeanMapper = mappers.getDocToBeanMapper(type);

		docs.forEachRemaining((d) -> {
			beans.add(docToBeanMapper.fromDocument(d));
		});
		return beans;
	}

	protected <T> T mapDocToBean(Document doc, Class<T> type) {
		DocToBeanMapper<T> docToBeanMapper = mappers.getDocToBeanMapper(type);
		return docToBeanMapper.fromDocument(doc);
	}

	protected <T> Iterable<T> asLazyIterable(FindIterable<Document> iterable, Class<T> type) {
		return new LazyIterable<>(iterable, type);
	}

	protected class LazyIterable<T> implements Iterable<T> {

		private FindIterable<Document> findIterable;
		private Class<T> type;

		public LazyIterable(FindIterable<Document> findIterable, Class<T> type) {
			this.findIterable = findIterable;
			this.type = type;
		}

		@Override
		public Iterator<T> iterator() {
			return new LazyIterator(findIterable.iterator(), type);
		}

		protected class LazyIterator implements Iterator<T> {

			private MongoCursor<Document> cursor;
			private Class<T> type;

			public LazyIterator(MongoCursor<Document> cursor, Class<T> type) {
				this.cursor = cursor;
				this.type = type;
			}

			@Override
			public boolean hasNext() {
				return cursor.hasNext();
			}

			@Override
			public T next() {
				Document document = cursor.next();
				return mapDocToBean(document, type);
			}

		}
	}

}
