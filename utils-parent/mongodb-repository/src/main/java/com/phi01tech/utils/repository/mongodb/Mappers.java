package com.phi01tech.utils.repository.mongodb;

public interface Mappers {

	<T> DocToBeanMapper<T> getDocToBeanMapper(Class<T> beanType);

	<T> BeanToDocMapper<T> getBeanToDocMapper(Class<T> beanType);

	Mappers NOP = new Mappers() {

		@Override
		public <T> DocToBeanMapper<T> getDocToBeanMapper(Class<T> beanType) {
			throw new UnsupportedOperationException();
		}

		@Override
		public <T> BeanToDocMapper<T> getBeanToDocMapper(Class<T> beanType) {
			throw new UnsupportedOperationException("Mapping is not supported");
		}

	};
}
