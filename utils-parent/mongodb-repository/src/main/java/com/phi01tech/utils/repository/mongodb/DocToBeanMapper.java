/**
 * 
 */
package com.phi01tech.utils.repository.mongodb;

import org.bson.Document;

/**
 * @author phi01tech
 *
 */
public interface DocToBeanMapper<T> {
	
	public T fromDocument(Document document);

}
