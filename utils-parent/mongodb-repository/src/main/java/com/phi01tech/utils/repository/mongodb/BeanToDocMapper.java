/**
 * 
 */
package com.phi01tech.utils.repository.mongodb;

import org.bson.Document;

/**
 * @author phi01tech
 *
 */
public interface BeanToDocMapper<T> {

	/**
	 * @param bean
	 * @return
	 */
	public Document fromBean(T bean);
}
