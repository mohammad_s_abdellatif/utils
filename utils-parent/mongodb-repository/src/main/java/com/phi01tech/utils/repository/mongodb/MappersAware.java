/**
 * 
 */
package com.phi01tech.utils.repository.mongodb;

/**
 * @author phi01tech
 *
 */
public interface MappersAware {
	
	void setMappers(Mappers mappers);
}
