/**
 * 
 */
package com.phi01tech.utils.repository.mongodb;

import java.util.Hashtable;
import java.util.Map;

/**
 * @author phi01tech
 *
 */
public class MappersRegistry implements Mappers {

	private Map<Class<?>, DocToBeanMapper<?>> docToBeanMappers = new Hashtable<>();
	private Map<Class<?>, BeanToDocMapper<?>> beanToDocMappers = new Hashtable<>();

	@Override
	@SuppressWarnings("unchecked")
	public <T> DocToBeanMapper<T> getDocToBeanMapper(Class<T> beanType) {
		Class<?> type = findClosestTypeInMap(beanType, docToBeanMappers);
		return (DocToBeanMapper<T>) docToBeanMappers.get(type);
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T> BeanToDocMapper<T> getBeanToDocMapper(Class<T> beanType) {
		Class<?> type = findClosestTypeInMap(beanType, beanToDocMappers);
		return (BeanToDocMapper<T>) beanToDocMappers.get(type);
	}

	public <T> boolean registerDocToBeanMapper(Class<? extends T> type, DocToBeanMapper<T> mapper) {
		if (mapper instanceof MappersAware) {
			((MappersAware) mapper).setMappers(this);
		}
		return docToBeanMappers.put(type, mapper) != null;
	}

	public <T> boolean registerBeanToDocMapper(Class<? extends T> type, BeanToDocMapper<T> mapper) {
		if (mapper instanceof MappersAware) {
			((MappersAware) mapper).setMappers(this);
		}
		return beanToDocMappers.put(type, mapper) != null;
	}

	private Class<?> findClosestTypeInMap(Class<?> beanType, Map<Class<?>, ? extends Object> map) {
		Class<?> type = beanType;
		while (type != null && !map.containsKey(type))
			type = type.getSuperclass();

		if (type == null) {
			Class<?>[] interfaces = beanType.getInterfaces();
			for (Class<?> interfaze : interfaces) {
				if (map.containsKey(interfaze)) {
					type = interfaze;
					break;
				}
			}
		}
		if (type == null)
			throw new IllegalArgumentException("unable to find mapper for type: " + beanType.getName());
		return type;
	}

}
