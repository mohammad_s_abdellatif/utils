/**
 * 
 */
package com.phi01tech.utils.date;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.junit.Assert;
import org.junit.Test;

import com.phi01tech.util.junit.gwt.GivenWhenThenTest;

/**
 * @author phi01tech
 *
 */
public class DateUtilsTest extends GivenWhenThenTest {

	@Test(expected = IllegalArgumentException.class)
	public void givenNullDate_whenConvertToLocalDate_thenFail() {
		DateUtils.toLocalDate(null);
	}

	@Test
	public void givenValidDate_whenConvertToLocalDate_thenSuccess() {
		given(Date::new)
				.whenReturningResult(DateUtils::toLocalDate)
				.then(this::assertLocalDateResult);
	}

	@Test
	public void givenNullDate_whenConvertToLocalDateTime_thenFail() {
		givenNull(Date.class)
				.whenReturningResult(DateUtils::toLocalDateTime)
				.thenThrowException(IllegalArgumentException.class);
	}

	@Test
	public void givenDate_whenConvertToLocalDateTime_thenSuccess() {
		given(Date::new)
				.whenReturningResult(DateUtils::toLocalDateTime)
				.then(this::assertLocalDateTimeResult);
	}

	@Test
	public void givenNullLocalDate_whenConvertLocalDateToDate_thenFail() {
		givenNull(LocalDate.class)
				.when(DateUtils::toDate)
				.thenThrowException(IllegalArgumentException.class);
	}

	@Test
	public void givenLocalDate_whenConvertLocalDateToDate_thenSuccess() {
		given(LocalDate::now)
				.whenReturningResult(DateUtils::toDate)
				.then((localDate, date) -> {
					LocalTime time = LocalTime.of(0, 0, 0);
					assertDateResult(localDate, time, date);
				});
	}

	@Test
	public void givenNullLocalDateTime_whenConvertLocalDateTimeToDate_thenSuccess() {
		givenNull(LocalDateTime.class)
				.whenReturningResult(DateUtils::toDate)
				.thenThrowException(IllegalArgumentException.class);
	}

	@Test
	public void givenLocalDateTime_whenConvertLocalDateTimeToDate_thenSuccess() {
		given(LocalDateTime::now)
				.whenReturningResult(DateUtils::toDate)
				.then((localDateTime, date) -> {
					Assert.assertNotNull(localDateTime);
					assertDateResult(localDateTime.toLocalDate(), localDateTime.toLocalTime(), date);
				});
	}

	@Test
	public void givenSQLDate_whenConvertDateToLocalDate_thenSuccess() {
		given(() -> new java.sql.Date(new Date().getTime()))
				.whenReturningResult(DateUtils::toLocalDate)
				.then(this::assertLocalDateResult);

	}

	private void assertLocalDateTimeResult(Date date, LocalDateTime localDateTime) {
		Assert.assertNotNull(localDateTime);
		assertLocalDateResult(date, localDateTime.toLocalDate());
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		calendar.setTime(date);
		Assert.assertEquals("invalid hour", calendar.get(Calendar.HOUR_OF_DAY), localDateTime.getHour());
		Assert.assertEquals("invalid mintue", calendar.get(Calendar.MINUTE), localDateTime.getMinute());
		Assert.assertEquals("invalid second", calendar.get(Calendar.SECOND), localDateTime.getSecond());
	}

	private void assertLocalDateResult(Date date, LocalDate localDate) {
		Assert.assertNotNull("result is null", localDate);
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		calendar.setTime(date);
		Assert.assertEquals("invalid date", calendar.get(Calendar.DATE), localDate.getDayOfMonth());
		Assert.assertEquals("invalid month", calendar.get(Calendar.MONTH) + 1, localDate.getMonthValue());
		Assert.assertEquals("invalid year", calendar.get(Calendar.YEAR), localDate.getYear());
	}

	private void assertDateResult(LocalDate localDate, LocalTime time, Date date) {
		Assert.assertNotNull("result is null", date);
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		calendar.setTime(date);
		Assert.assertEquals("invalid date", localDate.getDayOfMonth(), calendar.get(Calendar.DATE));
		Assert.assertEquals("invalid month", localDate.getMonthValue(), calendar.get(Calendar.MONTH) + 1);
		Assert.assertEquals("invalid year", localDate.getYear(), calendar.get(Calendar.YEAR));
		Assert.assertEquals("invalid hour", time.getHour(), calendar.get(Calendar.HOUR));
		Assert.assertEquals("invalid minute", time.getMinute(), calendar.get(Calendar.MINUTE));
		Assert.assertEquals("invalid second", time.getSecond(), calendar.get(Calendar.SECOND));
	}
}
