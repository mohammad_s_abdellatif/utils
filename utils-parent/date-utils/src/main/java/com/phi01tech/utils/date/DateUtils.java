package com.phi01tech.utils.date;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateUtils {

	public static LocalDate toLocalDate(Date date) {
		if (date == null)
			throw new IllegalArgumentException("date is null");
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));

		return LocalDate.of(calendar.get(Calendar.YEAR),
				calendar.get(Calendar.MONTH) + 1,
				calendar.get(Calendar.DATE));

	}

	public static LocalDateTime toLocalDateTime(Date date) {
		if (date == null)
			throw new IllegalArgumentException("date is null");
		Instant instant = date.toInstant();
		OffsetDateTime offsetDateTime = instant.atOffset(ZoneOffset.UTC);
		return offsetDateTime.toLocalDateTime();
	}

	public static Date toDate(LocalDate localDate) {
		if (localDate == null)
			throw new IllegalArgumentException("localdate is null");
		Instant instant = localDate.atStartOfDay(ZoneOffset.UTC).toInstant();
		return Date.from(instant);
	}

	public static Date toDate(LocalDateTime localDateTime) {
		if (localDateTime == null)
			throw new IllegalArgumentException("localdatetime is null");
		OffsetDateTime atOffset = localDateTime.atOffset(ZoneOffset.UTC);
		Instant instant = atOffset.toInstant();
		return Date.from(instant);
	}
}
