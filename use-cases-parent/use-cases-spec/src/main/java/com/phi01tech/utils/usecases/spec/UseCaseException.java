package com.phi01tech.utils.usecases.spec;

public class UseCaseException extends RuntimeException {

	public UseCaseException() {
		super();
	}

	public UseCaseException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public UseCaseException(String message, Throwable cause) {
		super(message, cause);
	}

	public UseCaseException(String message) {
		super(message);
	}

	public UseCaseException(Throwable cause) {
		super(cause);
	}

}
