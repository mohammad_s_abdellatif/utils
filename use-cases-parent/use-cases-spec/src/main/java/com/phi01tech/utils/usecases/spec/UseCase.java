package com.phi01tech.utils.usecases.spec;

public interface UseCase<I, R> {

	R execute(I input) throws UseCaseException;
}
